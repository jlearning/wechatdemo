package com.icoderoad.example.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
public class UserCaffeineApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserCaffeineApplication.class, args);
	}

}