package com.icoderoad.example.http2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SslHttp2Application {

    public static void main(String[] args) {
        SpringApplication.run(SslHttp2Application.class, args);
    }
}