package com.icoderoad.example.videom3u8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideoM3u8Application {
	
	public static void main(String[] args) {
		SpringApplication.run(VideoM3u8Application.class, args);
	}

}