package com.icoderoad.example.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NginxFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(NginxFileApplication.class, args);
	}

}