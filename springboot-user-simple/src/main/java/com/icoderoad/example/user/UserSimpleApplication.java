package com.icoderoad.example.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class UserSimpleApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserSimpleApplication.class, args);
	}

}